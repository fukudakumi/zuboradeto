class LinebotController < ApplicationController
  require 'line/bot'  # gem 'line-bot-api'

  # callbackアクションのCSRFトークン認証を無効
  protect_from_forgery :except => [:callback]

  def client
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def callback
    body = request.body.read # request body (webhook) を読み込む

  # 署名の検証(LINEからのリクエストであることを確認)
    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature) # 誤った署名が送られてきたら拒否する(対応しない)
      head :bad_request
    end

    # request bodyをパースして、扱いやすいクラスにマッピングする
    events = client.parse_events_from(body)
    prefectures = Prefecture.all
    dating ||= { prefecture: "" }

    # region(地域IDが入っていない場合)
    # webhookは１度のリクエストに複数のイベントが含まれているので１つ１つ処理する
    events.each { |event|
      @message = event.message['text'].gsub(" ", "") #ここでLINEで送った文章を取得。スペースなどの空白は削除
      # eventの種類による場合分け(MesageやBeaconなどがあります)
      case event
      when Line::Bot::Event::Message # Event::Messageの場合の処理
        case event.type
        when Line::Bot::Event::MessageType::Text # MessageType::Textの場合の処理
          case @message

          # spot関連
          when "北海道,東北"
            message = ::LineClient.second_reply_hokkaido_tohoku
            client.reply_message(event['replyToken'], message)
          when "関東"
            message = ::LineClient.second_reply_kanto
            client.reply_message(event['replyToken'], message)
          when "甲信越,東海"
            message = ::LineClient.second_reply_kousinnetu_tokai
            client.reply_message(event['replyToken'], message)
          when "関西,北陸"
            message = ::LineClient.second_reply_kansai_hokuriku
            client.reply_message(event['replyToken'], message)
          when "中国,四国"
            message = ::LineClient.second_reply_chugoku_shikoku
            client.reply_message(event['replyToken'], message)
          when "九州"
            message = ::LineClient.second_reply_kyusyu
            client.reply_message(event['replyToken'], message)
          when *prefectures.pluck(:name)
            prefecture = Prefecture.find_by(name: @message)
            message = ::LineClient.third_reply(prefecture.name)
            client.reply_message(event['replyToken'], message)
          when *prefectures.pluck(:name).map{ |name| name + "で遊ぶ!" }
            @prefecture = Prefecture.find_by(name: @message.match(/(.+?)で遊ぶ!$/)[1])
            @dating = Dating.new(walker_area: @prefecture.id)
            @walker_data = @dating.walker_data.shuffle
            random = rand(1..30)
            if random == 1
              message = ::LineClient.saboruna_reply
            else
              message = ::LineClient.fouth_reply(@walker_data)
            end
            client.reply_message(event['replyToken'], message)

          # food関連
          when /^(.+?)で、(.+?)$/
            @tabelog_area = "#{$1}"
            @tabelog_food = "#{$2}"
            message = ::LineClient.second_food_reply(@tabelog_area, @tabelog_food)
            client.reply_message(event['replyToken'], message)
          when /^(.+?)で(.+?)のお店を探す!$/
            @tabelog_area = "#{$1}"
            @tabelog_food = "#{$2}"
            @dating = Dating.new(tabelog_area: @tabelog_area, tabelog_food: @tabelog_food)
            @tabelog_data = @dating.tabelog_data.shuffle
            random = rand(1..30)
            if random == 1
              message = ::LineClient.saboruna_reply
            else
              message = ::LineClient.third_food_reply(@tabelog_data)
            end
            client.reply_message(event['replyToken'], message)

          # 共通質問（最初の部分）
          when "遊ぶ場所を決める!", "地域を選び直す", "やっぱり遊ぶ場所を決める!"
            message = ::LineClient.first_spot_message
            client.reply_message(event['replyToken'], message)
          when "ご飯の場所を決める!", "違う場所でご飯を探す", "やっぱり食べる場所を決める!"
            message = ::LineClient.first_food_message
            client.reply_message(event['replyToken'], message)
          else
            message = ::LineClient.first_message
            client.reply_message(event['replyToken'], message)
          end
        end
      end
    }


    head :ok # webhookに対するレスポンス
  end
end
