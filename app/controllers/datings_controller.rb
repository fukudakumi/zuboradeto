class DatingsController < ApplicationController

  def show
    @dating = Dating.new
  end

  def new
    @dating = Dating.new(dating_params)
  end

  def create

  end

  def regions_select
    if request.xhr?
      render partial: 'shared/prefecture', locals: {region_id: params[:region_id]}
    end
  end

  private

  def dating_params
    params.require(:dating).permit(:tabelog_area, :tabelog_food, :walker_area)
  end
end
