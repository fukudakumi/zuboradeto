$(document).on 'change', '#dating_region', ->
  $.ajax(
    type: 'GET'
    url: '/datings/regions_select'
    data: {
      region_id: $(this).val()
    }
  ).done (data) ->
    $('#dating_walker_area').html(data)
