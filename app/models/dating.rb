class Dating
  include ActiveModel::Model
  attr_accessor :tabelog_area, :tabelog_food, :walker_area, :region

  def tabelog_data
    agent = Mechanize.new

    # スクレイピング先のベースURL
    base_url = 'https://tabelog.com/'

    # 食べログトップページに遷移して、「場所」「種類」を入力。ランキングページへ移動。
    base_page = agent.get(base_url)
    form = base_page.forms[0]
    form.sa = self.tabelog_area
    form.sk = self.tabelog_food
    reference_page = agent.submit(form)
    rank_page = reference_page.link_with(:dom_class => "navi-rstlst__label navi-rstlst__label--rank").click

    array = Array.new
    # 食べログサイトの上から6個目の要素が空白のため、6を削除。
    [1,2,3,4,5,7,8,9,10,11].each do |n|
      h = Hash.new
      h[:name]        = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[1]/div/div/div/a").text
      h[:image]       = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[2]/div[3]/div/p/a/img").attribute("data-original").value
      h[:url]         = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[2]/div[3]/div/p/a").attribute("href").text
      h[:kutikomi]    = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[2]/div[2]/a/strong").text
      h[:holiday]     = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[2]/div[1]/div/div[2]/dl/dd/span[1]").text
      h[:rating]    = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[2]/div[1]/div/div[1]/p[1]/span").text
      h[:place]    = rank_page.search("//*[@id=\"column-main\"]/ul/li[#{n}]/div[2]/div[1]/div/div/div/span/text()[1]").text

      unless h[:name].blank? || h[:image].blank? || h[:url].blank? || h[:rating].blank? || h[:place].blank?
        array << h
      end
    end

    array
  end

  def walker_data
    agent = Mechanize.new

    # スクレイピング先のベースURL
    base_url = 'https://www.walkerplus.com/ranking/event/'
    # いったん東京で指定
    location_url = "/" + Prefecture.find(self.walker_area).area

    rank_url = base_url + location_url
    rank_page = agent.get(rank_url)

    array = Array.new
    1.upto(10) do |n|
      h = Hash.new
      h[:name]        = rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/div[1]/a[1]").text
      h[:discription] = rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/div[1]/a[2]").text
      h[:image]       = "https:" + rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/a/span/img").attribute("src").value
      h[:url]         = "https://www.walkerplus.com" + rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/a").attribute("href").text
      h[:date]        = rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/div[2]/p[1]").text
      h[:location]    = rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/div[2]/p[2]/text()").text.chomp.strip
      h[:place]       = rank_page.search("/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[#{n}]/div/div[2]/p[3]").text.chomp.strip

        array << h
    end

    array
  end

end
