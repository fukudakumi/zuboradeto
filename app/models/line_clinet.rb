class LineClient
  # 1回目の質問
  def self.first_message
    { "type": "text",
                    "text": "何を決める？",
                    "quickReply": {
                      "items":
                      [
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "遊ぶ場所を決める!",
                          "text": "遊ぶ場所を決める!"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "ご飯の場所を決める!",
                          "text": "ご飯の場所を決める!"
                          }
                        }
                      ]
                    }
                  }
  end

  def self.first_spot_message
    { "type": "text",
                    "text": "どこの地域で遊ぶ？",
                    "quickReply": {
                      "items":
                      [
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "北海道,東北",
                          "text": "北海道,東北"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "関東",
                          "text": "関東"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "甲信越,東海",
                          "text": "甲信越,東海"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "関西,北陸",
                          "text": "関西,北陸"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "中国,四国",
                          "text": "中国,四国"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                            "type": "message",
                            "label": "九州",
                            "text": "九州"
                            }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "やっぱり食べる場所を決める!",
                          "text": "やっぱり食べる場所を決める!"
                          }
                        }
                      ]
                    }
                  }
  end

  def self.first_food_message
    { "type": "text",
                    "text": "どこで食べる？\n\n【入力方法】\n「〇〇で、□□」と入力\n例：新宿で、とんかつ",
                    "quickReply": {
                      "items":
                      [
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "やっぱり遊ぶ場所を決める!",
                          "text": "やっぱり遊ぶ場所を決める!"
                          }
                        }
                      ]
                    }
                  }
  end

  # spot 2回目の質問
  def self.second_reply_hokkaido_tohoku
    { "type": "text",
                    "text": "どの県で遊ぶの？",
                    "quickReply": {
                      "items":
                      [
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "北海道",
                          "text": "北海道"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "宮城県",
                          "text": "宮城県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                            "type": "message",
                            "label": "青森県",
                            "text": "青森県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "岩手県",
                          "text": "岩手県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "秋田県",
                          "text": "秋田県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "山形県",
                          "text": "山形県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "福島県",
                          "text": "福島県"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "地域を選び直す",
                          "text": "地域を選び直す"
                          }
                        }
                      ]
                    }
                  }
  end

  def self.second_reply_kanto
     { "type": "text",
                  "text": "どの県で遊ぶの？",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "東京都",
                        "text": "東京都"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "神奈川県",
                          "text": "神奈川県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "千葉県",
                        "text": "千葉県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "埼玉県",
                        "text": "埼玉県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "群馬県",
                        "text": "群馬県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "栃木県",
                        "text": "栃木県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "茨城県",
                        "text": "茨城県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  def self.second_reply_kousinnetu_tokai
     { "type": "text",
                  "text": "どの県で遊ぶの？",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "山梨県",
                        "text": "山梨県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "長野県",
                          "text": "長野県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "新潟県",
                        "text": "新潟県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "愛知県",
                        "text": "愛知県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "岐阜県",
                          "text": "岐阜県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "三重県",
                        "text": "三重県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "静岡県",
                        "text": "静岡県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  def self.second_reply_kansai_hokuriku
     { "type": "text",
                  "text": "どの県で遊ぶの？",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "大阪府",
                        "text": "大阪府"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "京都府",
                          "text": "京都府"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "兵庫県",
                        "text": "兵庫県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "奈良県",
                        "text": "奈良県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "和歌山県",
                        "text": "和歌山県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "滋賀県",
                        "text": "滋賀県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "石川県",
                        "text": "石川県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "富山県",
                          "text": "富山県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "福井県",
                        "text": "福井県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  def self.second_reply_chugoku_shikoku
     { "type": "text",
                  "text": "どの県で遊ぶの？",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "広島県",
                        "text": "広島県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "岡山県",
                          "text": "岡山県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "山口県",
                        "text": "山口県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "鳥取県",
                        "text": "鳥取県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "島根県",
                        "text": "島根県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "香川県",
                        "text": "香川県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "愛媛県",
                          "text": "愛媛県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "徳島県",
                        "text": "徳島県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "高知県",
                        "text": "高知県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  def self.second_reply_kyusyu
     { "type": "text",
                  "text": "どの県で遊ぶの？",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "福岡県",
                        "text": "福岡県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                          "type": "message",
                          "label": "佐賀県",
                          "text": "佐賀県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "長崎県",
                        "text": "長崎県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "熊本県",
                        "text": "熊本県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "大分県",
                        "text": "大分県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "宮崎県",
                        "text": "宮崎県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "鹿児島県",
                        "text": "鹿児島県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "沖縄県",
                        "text": "沖縄県"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  # spot 3回目の質問
  def self.third_reply(prefecture)
     { "type": "text",
                  "text": "#{prefecture}で遊ぶでいい?",
                  "quickReply": {
                    "items":
                    [
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "#{prefecture}で遊ぶ!",
                        "text": "#{prefecture}で遊ぶ!"
                        }
                      },
                      {
                        "type": "action",
                        "action": {
                        "type": "message",
                        "label": "地域を選び直す",
                        "text": "地域を選び直す"
                        }
                      }
                    ]
                  }
                }
  end

  # spot 4回目の質問
  def self.fouth_reply(walker_data)
    {  "type": "flex",
                "altText": "オススメのデートスポット",
                "contents": {
                  "type": "carousel",
                  "contents": [
                  {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{walker_data[0][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{walker_data[0][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{walker_data[0][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '日程',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[0][:date]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '開催地',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[0][:location]}-#{walker_data[0][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '説明',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[0][:discription]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                },
                {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{walker_data[1][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{walker_data[1][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{walker_data[1][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '日程',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[1][:date]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '開催地',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[1][:location]}-#{walker_data[1][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '説明',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[1][:discription]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                },
                {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{walker_data[2][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{walker_data[2][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{walker_data[2][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '日程',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[2][:date]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '開催地',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[2][:location]}-#{walker_data[2][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '説明',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{walker_data[2][:discription]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                }
              ]
            },
            "quickReply": {
              "items":
              [
                {
                  "type": "action",
                  "action": {
                  "type": "message",
                  "label": "地域を選び直す",
                  "text": "地域を選び直す"
                  }
                },
                {
                  "type": "action",
                  "action": {
                  "type": "message",
                  "label": "ご飯の場所を決める!",
                  "text": "ご飯の場所を決める!"
                  }
                }
              ]
            }
          }
  end

  # food 2回目の質問
  def self.second_food_reply(tabelog_area, tabelog_food)
    {
      "type": "text",
                      "text": "#{tabelog_area}で#{tabelog_food}のお店を探す？",
                      "quickReply": {
                        "items":
                        [
                          {
                            "type": "action",
                            "action": {
                            "type": "message",
                            "label": "#{tabelog_area}で#{tabelog_food}のお店を探す!",
                            "text": "#{tabelog_area}で#{tabelog_food}のお店を探す!"
                            }
                          },
                          {
                            "type": "action",
                            "action": {
                            "type": "message",
                            "label": "違う場所でご飯を探す",
                            "text": "違う場所でご飯を探す"
                            }
                          },
                          {
                            "type": "action",
                            "action": {
                            "type": "message",
                            "label": "やっぱり遊ぶ場所を決める!",
                            "text": "やっぱり遊ぶ場所を決める!"
                            }
                          }
                        ]
                      }
    }
  end

  # food 3回目の質問
  def self.third_food_reply(tabelog_data)
    {  "type": "flex",
                "altText": "オススメのご飯屋さん",
                "contents": {
                  "type": "carousel",
                  "contents": [
                  {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{tabelog_data[0][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{tabelog_data[0][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{tabelog_data[0][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '評価',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "☆#{tabelog_data[0][:rating]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '場所',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[0][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '定休日',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[0][:holiday]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                  },
                  {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{tabelog_data[1][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{tabelog_data[1][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{tabelog_data[1][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '評価',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "☆#{tabelog_data[1][:rating]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '場所',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[1][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '定休日',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[1][:holiday]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                  },
                  {
                  "type": 'bubble',
                  "hero": {
                    "type": "image",
                    "size": "full",
                    "aspectRatio": "20:13",
                    "aspectMode": "cover",
                    "url": "#{tabelog_data[2][:image]}",
                    "action": {
                      "type": "uri",
                      "uri": "#{tabelog_data[2][:url]}"
                    }
                  },
                  "body": {
                    "type": 'box',
                    "layout": 'vertical',
                    "spacing": 'md',
                    "contents": [
                      {
                        "type": 'text',
                        "text": "#{tabelog_data[2][:name]}",
                        "wrap": true,
                        "weight": 'bold',
                        "gravity": 'center',
                        "size": 'xl'
                      },
                      {
                        "type": 'box',
                        "layout": 'vertical',
                        "margin": 'lg',
                        "spacing": 'sm',
                        "contents": [
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '評価',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "☆#{tabelog_data[2][:rating]}",
                                "wrap": true,
                                "size": 'sm',
                                "color": '#666666',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '場所',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[2][:place]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          },
                          {
                            "type": 'box',
                            "layout": 'baseline',
                            "spacing": 'sm',
                            "contents": [
                              {
                                "type": 'text',
                                "text": '定休日',
                                "color": '#aaaaaa',
                                "size": 'sm',
                                "flex": 1
                              },
                              {
                                "type": 'text',
                                "text": "#{tabelog_data[2][:holiday]}",
                                "wrap": true,
                                "color": '#666666',
                                "size": 'sm',
                                "flex": 4
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                  }
                ]
              },
              "quickReply": {
                "items":
                [
                  {
                    "type": "action",
                    "action": {
                    "type": "message",
                    "label": "違う場所でご飯を探す",
                    "text": "違う場所でご飯を探す"
                    }
                  },
                  {
                    "type": "action",
                    "action": {
                    "type": "message",
                    "label": "遊ぶ場所を決める!",
                    "text": "遊ぶ場所を決める!"
                    }
                  }
                ]
              }
            }
  end

  # 共通の最後
  def self.saboruna_reply
    { "type": "text",
                    "text": "サボるな\n自分でググれカス",
                    "quickReply": {
                      "items":
                      [
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "ごめん",
                          "text": "ごめん"
                          }
                        },
                        {
                          "type": "action",
                          "action": {
                          "type": "message",
                          "label": "うるさい、しばく",
                          "text": "うるさい、しばく"
                          }
                        }
                      ]
                    }
                  }
  end



end
