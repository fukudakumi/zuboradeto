namespace :tabelog_dating do

  desc '食べログからデートに必要な情報を取得'
  task :tabelog_data, ['name'] => :environment do |task, args|

    agent = Mechanize.new
    agent.user_agent = 'Mac Safari'

    # スクレイピング先のベースURL
    base_url = 'https://tabelog.com/'
    login_url = base_url + '/login'
    my_url = base_url + '/u/YOUR_ID'
    favorite_url = my_url + '/favorite_user'

    # 食べログトップページに遷移して、「場所」「種類」を入力。ランキングページへ移動。
    base_page = agent.get(base_url)
    form = base_page.forms[0]
    form.sa = '新宿駅'
    form.sk = 'とんかつ'
    reference_page = agent.submit(form)
    rank_page = reference_page.link_with(:dom_class => "navi-rstlst__label navi-rstlst__label--rank").click

    # 店名の1位、2位、3位は下記のようになる。
    # rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[1]/div/div[2]/div/a')
    # rank_page.search('//*[@id="column-main"]/ul/li[2]/div[2]/div[1]/div/div[2]/div/a')
    # rank_page.search('//*[@id="column-main"]/ul/li[3]/div[2]/div[1]/div/div[2]/div/a')

    # 画像の1位、2位、3位は下記のようになる。
    # doc_image = rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[2]/div[3]/div/p/a/img')
    # doc_image = rank_page.search('//*[@id="column-main"]/ul/li[2]/div[2]/div[2]/div[3]/div/p/a/img')
    # doc_image = rank_page.search('//*[@id="column-main"]/ul/li[3]/div[2]/div[2]/div[3]/div/p/a/img')

    # doc_names = rank_page.search('//*[@id="column-main"]/ul/li/div/div/div/div/div/a')
    # doc_images = rank_page.search('//*[@id="column-main"]/ul/li/div/div/div/div/p/a/img')

    # names = []
    # doc_names.each do |node|
    #  names << node.text
    #end

    #images = []
    #doc_images.each do |node|
    #  images << node.attribute("data-original").value
    #end

    p rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[1]/div/div[2]/div/a')
    p rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[2]/div[3]/div/p/a/img')
#    name = ""
#    doc.each do |node|
#      name = node.text
#    end

#    p name

#    agent.get(base_url) do |page|
#      page.form_with()
#    end




#    charset = nil
#    html = open(url) do |f|
#      charset = f.charset # 文字種別を取得
#      f.read # htmlを読み込んで変数htmlに渡す
#    end

    # htmlをパース(解析)してオブジェクトを作成
#    doc = Nokogiri::HTML.parse(html, nil, charset)

#    arr = Array.new
#    doc.css('li.mdTopMTMList01Item').each do |row|
#      arr << [row.css('h3').inner_text,
#              row.css('div.mdTopMTMList01Option').css('p.mdTopMTMList01UserName').text
#            ]
#    end


#    p args
#    p args.name

#    tabelogScrapeFile = "db/tabelog/" + DateTime.now.strftime('%Y%m%d%H%M%S') + "-" + args.name + "-" + "tabelog-" + ".csv"

#    File.open(tabelogScrapeFile, "w") do |csv|
#      arr.each do |lineArray|
#        lineArray.each do |i|
#          csv.print(i)
#          csv.print(",")
#        end
#        csv.print "\n"
#      end
#    end
  end
end
