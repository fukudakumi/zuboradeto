namespace :walker_dating do

  desc 'Walkerからデートに必要な情報を取得'
  task :walker_data, ['name'] => :environment do |task, args|

    agent = Mechanize.new
    agent.user_agent = 'Mac Safari'

    # スクレイピング先のベースURL
    base_url = 'https://www.walkerplus.com/ranking/event/'
    # いったん東京で指定
    location_url = 'ar0313/'

    rank_url = base_url + location_url
    rank_page = agent.get(rank_url)

    p rank_page.uri
    p rank_page.search('/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[1]/div/div[1]/a[1]').text
    p rank_page.search('/html/body/div/div[1]/main/section/div[4]/div/div/ul/li[1]/div/a/span/img').attribute("src").value

    #rank_page = reference_page.link_with(:dom_class => "navi-rstlst__label navi-rstlst__label--rank").click


    #p rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[1]/div/div[2]/div/a')
    #p rank_page.search('//*[@id="column-main"]/ul/li[1]/div[2]/div[2]/div[3]/div/p/a/img')
  end
end
