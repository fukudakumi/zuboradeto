class Prefectures < ActiveRecord::Migration[5.2]
  def change
    create_table :prefectures do |t|
      t.string :name
      t.integer :region_id
      t.string :area

      t.timestamps
    end
  end
end
