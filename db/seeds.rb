# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#regionの作成
regions = [["北海道", 1], ["東北", 2], ["関東", 3], ["甲信越", 4], ["東海", 5], ["北陸", 6], ["関西", 7], ["中国", 8], ["四国", 9], ["九州", 10]]
regions.each do |region|
  Region.create!(
    name: region[0],
#    id: region[1]
  )
end



# prefectureの作成
hokkaido = ["北海道", 1, "ar0101"]
tohoku = [["宮城県", 2, "ar0204"], ["青森県", 3, "ar0202"], ["岩手県", 4, "ar0203"], ["秋田県", 5, "ar0205"], ["山形県", 6, "ar0206"], ["福島県", 7, "ar0207"]]
kanto = [["東京都", 8, "ar0313"], ["神奈川県", 9, "ar0314"], ["千葉県", 10, "ar0312"], ["埼玉県", 11, "ar0311"], ["群馬県", 12, "ar0310"], ["栃木県", 13, "ar0309"], ["茨城県", 14, "ar0308"]]
kousinnetu = [["山梨県", 15, "ar0419"], ["長野県", 16, "ar0420"], ["新潟県", 17, "ar0415"]]
tokai = [["愛知県", 18, "ar0623"], ["岐阜県", 19, "ar0621"], ["三重県", 20, "ar0624"], ["静岡県", 21, "ar0622"]]
hokuriku = [["石川県", 22, "ar0517"], ["富山県", 23, "ar0516"], ["福井県", 24, "ar0518"]]
kansai = [["大阪府", 25, "ar0727"], ["京都府", 26, "ar0726"], ["兵庫県", 27, "ar0728"], ["奈良県", 28, "ar0729"], ["和歌山県", 29, "ar0730"], ["滋賀県", 30, "ar0725"]]
chugoku = [["広島県", 31, "ar0834"], ["岡山県", 32, "ar0833"], ["山口県", 33, "ar0835"], ["鳥取県", 34, "ar0831"], ["島根県", 35, "ar0832"]]
sikoku = [["香川県", 36, "ar0937"], ["愛媛県", 37, "ar0938"], ["徳島県", 38, "ar0936"], ["高知県", 39, "ar0939"]]
kyusyu = [["福岡県", 40, "ar1040"], ["佐賀県", 41, "ar1041"], ["長崎県", 42, "ar1042"], ["熊本県", 43, "ar1043"], ["大分県", 44, "ar1044"], ["宮崎県", 45, "ar1045"], ["鹿児島県", 46, "ar1046"], ["沖縄県", 47, "ar1047"]]


Prefecture.create!(
  name: hokkaido[0],
  area: hokkaido[2],
  region_id: 1
)

tohoku.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 2
  )
end

kanto.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 3
  )
end

kousinnetu.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 4
  )
end

tokai.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 5
  )
end

hokuriku.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 6
  )
end

kansai.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 7
  )
end

chugoku.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 8
  )
end

sikoku.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 9
  )
end

kyusyu.each do |prefecture|
  Prefecture.create!(
    name: prefecture[0],
    area: prefecture[2],
    region_id: 10
  )
end
